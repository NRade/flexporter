﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace FlexPorter.Models
{
	public class ExcelImportOptions
	{
		public bool excludeSheetsMode;
		public string[] SheetNames;
		public int[] SheetIndexes = new int[] { 0 };
		public int? EnoughHeaderMatches = 3;
		public int? RowsToScanForHeaders = 100;

		public Type ObjectType
		{
			set => CandidateTypes = new List<Type> { value };
			get => CandidateTypes.SingleOrDefault();
		}

		public List<Type> CandidateTypes = new List<Type>();
	}
}
