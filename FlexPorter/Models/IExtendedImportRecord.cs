﻿using System.Collections.Generic;

namespace FlexPorter.Models
{
	public interface IExtendedExcelImportRecord : IExcelImportRecord
	{
		Dictionary<string, object> OtherValues { get; set; }
	}
}
