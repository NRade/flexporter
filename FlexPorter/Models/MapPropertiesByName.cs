﻿using System;

namespace FlexPorter.Models
{
	/// <summary>
	/// map all properties in the class to excel table header names based on PascalCase naming convention
	/// </summary>
	public class MapByPascalCaseName : Attribute
	{
	}
}
