﻿namespace FlexPorter.Models
{
	public interface IMappedExcelImportRecord : IExcelImportRecord
	{
		string MapPropertyName(string headerName);
	}
}
