﻿using System;

namespace FlexPorter.Models
{
	public class ExcelImportProperty : Attribute
	{
		public string HeaderName { get; set; }

		public ExcelImportProperty(string headerName)
		{
			HeaderName = headerName;
		}
	}
}
