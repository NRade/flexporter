﻿using System;

namespace FlexPorter.Models
{
	public interface ITransformableExcelImportRecord : IExcelImportRecord
	{
		object TransformPropertyValue(object input, Type type);
	}
}
